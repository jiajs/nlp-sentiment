package eshore.cn.it.classification;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.NumberFormat;
import java.util.List;

import com.aliasi.classify.Classification;
import com.aliasi.classify.Classified;
import com.aliasi.classify.ConfusionMatrix;
import com.aliasi.classify.ScoredClassification;
import com.aliasi.classify.ScoredClassifier;
import com.aliasi.classify.TfIdfClassifierTrainer;
import com.aliasi.tokenizer.CharacterTokenizerFactory;
import com.aliasi.tokenizer.TokenFeatureExtractor;
import com.aliasi.util.Files;
import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.seg.common.Term;

/**
 * 基于LingPipe的文本分类器，主要分类成两类
 * 一类：		关于政务的
 * 另一类：	非政务的
 * 采用的算法有：
 * @author  clebeg
 * @time	2015-04-13
 * */
public class DfIdfClassierTest {
	private static String[] CATEGORIES = {
		"government",
		"others"
	};
	
	private static String TEXT_CLASSIFICATION_TRAINING = "data/text_classification/training";
	private static String TEXT_CLASSIFICATION_TESTING = "data/text_classification/testing";
	private static String MODEL_FILE = "Model/tfidf_classifier"; 
	
	private static TfIdfClassifierTrainer<CharSequence> classifier = new TfIdfClassifierTrainer<CharSequence>(  
            new TokenFeatureExtractor(CharacterTokenizerFactory.INSTANCE)); 
	
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		trainModel(true);
		evaluate();
	}
	public static void trainModel(boolean needStoreModle) throws IOException {
		for(int i = 0; i < CATEGORIES.length; ++i) {
            File classDir = new File(TEXT_CLASSIFICATION_TRAINING, CATEGORIES[i]);
            if (!classDir.isDirectory()) {
                String msg = "Could not find training directory="
                    + classDir
                    + "\nHave you unpacked "
                    + CATEGORIES.length
                    + "groups?";
                System.out.println(msg); // in case exception gets lost in shell
                throw new IllegalArgumentException(msg);
            }

            String[] trainingFiles = classDir.list();
            for (int j = 0; j < trainingFiles.length; ++j) {
                File file = new File(classDir, trainingFiles[j]);
                String text = Files.readFromFile(file, "GBK");
                System.out.println("Training on " + CATEGORIES[i] + "/" + trainingFiles[j]);
                
                String segWords = "";
        		List<Term> terms = HanLP.segment(text);
        		for (Term term : terms)
        			segWords += term.word + " ";
        		
                Classification classification
                    = new Classification(CATEGORIES[i]);
                Classified<CharSequence> classified
                    = new Classified<CharSequence>(segWords, classification);
                classifier.handle(classified);
            }
        }
		if (needStoreModle) {
			System.out.println("开始保存分类器到  " + MODEL_FILE);  
	        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(  
	        		MODEL_FILE));  
	        classifier.compileTo(os);  
	        os.close();  
	        System.out.println("分类器保存完成");  
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void evaluate() throws IOException, ClassNotFoundException {
		ScoredClassifier<CharSequence> compiledClassifier = null;  
        try {  
            ObjectInputStream oi = new ObjectInputStream(new FileInputStream(  
            		MODEL_FILE));  
            compiledClassifier = (ScoredClassifier<CharSequence>) oi  
                    .readObject();  
            oi.close();  
        } catch (IOException ie) {  
            System.out.println("IO Error: Model file " + MODEL_FILE + " missing");  
        }  
        // 遍历分类目录中的文件测试分类准确度  
        ConfusionMatrix confMatrix = new ConfusionMatrix(CATEGORIES);  
        NumberFormat nf = NumberFormat.getInstance();  
        nf.setMaximumIntegerDigits(1);  
        nf.setMaximumFractionDigits(3);  
        for(int i = 0; i < CATEGORIES.length; ++i) {
            File classDir = new File(TEXT_CLASSIFICATION_TESTING, CATEGORIES[i]);
            String[] testingFiles = classDir.list();
            for (int j = 0; j < testingFiles.length;  ++j) {
                String text
                    = Files.readFromFile(new File(classDir, testingFiles[j]),"GBK");
                String segWords = "";
        		List<Term> terms = HanLP.segment(text);
        		for (Term term : terms)
        			segWords += term.word + " ";
                
        		System.out.println("测试 " + CATEGORIES[i]  
                        + File.separator + testingFiles[j]);  
  
                ScoredClassification classification = compiledClassifier  
                        .classify(segWords.subSequence(0, segWords.length()));  
                confMatrix.increment(CATEGORIES[i],  
                        classification.bestCategory());  
                System.out.println("最适合的分类: "  
                        + classification.bestCategory()); 
            }
        }
        
        System.out.println("--------------------------------------------");  
        System.out.println("- 结果 ");  
        System.out.println("--------------------------------------------");  
        int[][] imatrix = confMatrix.matrix();  
        StringBuffer sb = new StringBuffer();  
        sb.append(StringTools.fillin("CATEGORY", 10, true, ' '));  
        for (int i = 0; i < CATEGORIES.length; i++)  
            sb.append(StringTools.fillin(CATEGORIES[i], 8, false, ' '));  
        System.out.println(sb.toString());  
  
        for (int i = 0; i < imatrix.length; i++) {  
            sb = new StringBuffer();  
            sb.append(StringTools.fillin(CATEGORIES[i], 10, true, ' ',  
                    10 - CATEGORIES[i].length()));  
            for (int j = 0; j < imatrix.length; j++) {  
                String out = "" + imatrix[i][j];  
                sb.append(StringTools.fillin(out, 8, false, ' ',  
                        8 - out.length()));  
            }  
            System.out.println(sb.toString());  
        }  
  
        System.out.println("准确度: "  
                + nf.format(confMatrix.totalAccuracy()));  
        System.out.println("总共正确数 : " + confMatrix.totalCorrect());  
        System.out.println("总数：" + confMatrix.totalCount());  
	}
}
