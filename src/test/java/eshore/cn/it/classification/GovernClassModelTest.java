package eshore.cn.it.classification;

import java.io.IOException;

import eshore.cn.it.configuration.ClassModelConfiguration;
import junit.framework.TestCase;
/***
 * 测试建立 与政府相关 以及 与政府不相关的 分类器
 * @author clebeg 2015-04-14 11:45
 */
public class GovernClassModelTest extends TestCase {
	private String[] CATEGORIES = {
		"government",
		"others"
	};
	
	public void trainModel() {
		ClassModelConfiguration modelConfig = new ClassModelConfiguration();
		modelConfig.setCategories(CATEGORIES);
		modelConfig.setTrainRootDir("data/text_classification/training");
		modelConfig.setModelFile("C:/model/mymodel.class");
		
		NGramClassierTrainer ngct = new NGramClassierTrainer();
		ngct.setNgramSize(3);
		ngct.setModelConfig(modelConfig);
		try {
			ngct.trainModel();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
